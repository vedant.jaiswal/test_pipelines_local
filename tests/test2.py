def mark_dirty(self, event=None):
        start_index = "1.0"
        end_index = "end"

        if hasattr(event, "sequence"):
            if event.sequence == "TextInsert":
                index = self.text.index(event.index)
                start_row = int(index.split(".")[0])
                end_row = start_row + event.text.count("\n")
                start_index = "%d.%d" % (start_row, 0)
                end_index = "%d.%d" % (end_row + 1, 0)
                if not event.trivial_for_coloring:
                    self._multiline_dirty = True

            elif event.sequence == "TextDelete":
                index = self.text.index(event.index1)
                start_row = int(index.split(".")[0])
                start_index = "%d.%d" % (start_row, 0)
                end_index = "%d.%d" % (start_row + 1, 0)
                if not event.trivial_for_coloring:
                    self._multiline_dirty = True

        self.text.tag_add(TODO, start_index, end_index) 
# Not recommended
fn = 'John'
Ln = "Doe"
cre_tmstp = 1621535852
print("Hello")
